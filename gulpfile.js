//
// Use:
// gulp --pathToVendor "/app/pim/vendor/" --pathToForeignBundle "kitt3n/pimcore-elements/" --pathInsideForeignBundle "src/KITT3N/Pimcore/ElementsBundle/"
// gulp --pathToVendor "/app/pim/vendor/" --pathToForeignBundle "kitt3n/pimcore-layouts/" --pathInsideForeignBundle "src/KITT3N/Pimcore/LayoutsBundle/"
//

// fetch command line arguments
const arg = (argList => {

    let arg = {}, a, opt, thisOpt, curOpt;
    for (a = 0; a < argList.length; a++) {

        thisOpt = argList[a].trim();
        opt = thisOpt.replace(/^\-+/, '');

        if (opt === thisOpt) {

            // argument value
            if (curOpt) arg[curOpt] = opt;
            curOpt = null;

        }
        else {

            // argument name
            curOpt = opt;
            arg[curOpt] = true;

        }

    }

    return arg;

})(process.argv);

console.log('--pathToVendor ' + arg.pathToVendor);
console.log('--pathToForeignBundle' + arg.pathToForeignBundle);
console.log('--pathInsideForeignBundle ' + arg.pathInsideForeignBundle);

/**
 * Includes
 */
var gulp = require('gulp');
var sass = require('gulp-sass');
var cleanCSS = require('gulp-clean-css');
var autoprefixer = require('gulp-autoprefixer');
var rename = require('gulp-rename');
var concat = require('gulp-concat');
// var uglify = require('gulp-uglify');
// var wrap = require('gulp-wrap');
// var tap = require('gulp-tap');
// var util = require('gulp-util');
// var debug = require('gulp-debug');
// var path = require('path');

var pathToVendor = arg.pathToVendor;
var pathToForeignBundle = arg.pathToForeignBundle;
var pathInsideForeignBundle = arg.pathInsideForeignBundle;

var pathToCustomBundle = "kitt3n/pimcore-custom/";
var pathInsideCustomBundle = "src/KITT3N/Pimcore/CustomBundle/";

/**
 * Scss => Css
 */
gulp.task('viewCss', function () {
    gulp.src(
        [
            pathToVendor + pathToCustomBundle + pathInsideCustomBundle + 'Resources/private/assets/shared/scss/**/*.scss',
            pathToVendor + pathToCustomBundle + pathInsideCustomBundle + 'Resources/private/assets/view/scss/**/*.scss',
            pathToVendor + pathToForeignBundle + pathInsideForeignBundle +'/Resources/private/assets/view/scss/*.scss'
        ]
    )
        .pipe(
        concat('view.css'))
        .pipe(sass({
            includePaths: [
                pathToVendor + pathToForeignBundle
            ]
        }).on('error', sass.logError))
        .pipe(
            gulp.dest(
                pathToVendor + pathToCustomBundle + pathInsideCustomBundle + 'Resources/public/assets/view/css/' +  pathToForeignBundle
            )
        )
        .pipe(
            rename('view.min.css')
        )
        .pipe(
            autoprefixer({
            browsers: ['last 3 versions', 'safari 7', 'safari 8', 'edge 15', 'ie 9-11'],
            grid: true
        }))
        .pipe(cleanCSS(
            {
                level: {
                    2: {
                        // mergeAdjacentRules: false, // controls adjacent rules merging; defaults to true
                        // mergeIntoShorthands: false, // controls merging properties into shorthands; defaults to true
                        mergeMedia: false, // controls `@media` merging; defaults to true
                        mergeNonAdjacentRules: false // controls non-adjacent rule merging; defaults to true
                        // mergeSemantically: true // controls semantic merging; defaults to false
                        // overrideProperties: true, // controls property overriding based on understandability; defaults to true
                        // reduceNonAdjacentRules: true, // controls non-adjacent rule reducing; defaults to true
                        // removeDuplicateFontRules: true, // controls duplicate `@font-face` removing; defaults to true
                        // removeDuplicateMediaBlocks: true, // controls duplicate `@media` removing; defaults to true
                        // removeDuplicateRules: true, // controls duplicate rules removing; defaults to true
                        // restructureRules: true // controls rule restructuring; defaults to false
                    }

                }
            }
        ))
        .pipe(
            gulp.dest(
                pathToVendor + pathToCustomBundle + pathInsideCustomBundle + 'Resources/public/assets/view/css/' +  pathToForeignBundle
            )
        )
});

/**
 * Scss => Css
 */
gulp.task('editCss', function () {
    gulp.src(
        [
            pathToVendor + pathToCustomBundle + pathInsideCustomBundle + 'Resources/private/assets/shared/scss/**/*.scss',
            pathToVendor + pathToCustomBundle + pathInsideCustomBundle + 'Resources/private/assets/edit/scss/**/*.scss',
            pathToVendor + pathToForeignBundle + pathInsideForeignBundle + '/Resources/private/assets/edit/scss/*.scss'
        ]
    )
    .pipe(
        concat('edit.css'))
    .pipe(sass({
        includePaths: [
            pathToVendor + pathToForeignBundle
        ]
    }).on('error', sass.logError))
    .pipe(
        gulp.dest(
            pathToVendor + pathToCustomBundle + pathInsideCustomBundle + 'Resources/public/assets/edit/css/' +  pathToForeignBundle
        )
    )
    .pipe(
        rename('edit.min.css')
    )
    .pipe(
        autoprefixer({
            browsers: ['last 3 versions', 'safari 7', 'safari 8', 'edge 15', 'ie 9-11'],
            grid: true
        }))
    .pipe(cleanCSS(
        {
            level: {
                2: {
                    // mergeAdjacentRules: false, // controls adjacent rules merging; defaults to true
                    // mergeIntoShorthands: false, // controls merging properties into shorthands; defaults to true
                    mergeMedia: false, // controls `@media` merging; defaults to true
                    mergeNonAdjacentRules: false // controls non-adjacent rule merging; defaults to true
                    // mergeSemantically: true // controls semantic merging; defaults to false
                    // overrideProperties: true, // controls property overriding based on understandability; defaults to true
                    // reduceNonAdjacentRules: true, // controls non-adjacent rule reducing; defaults to true
                    // removeDuplicateFontRules: true, // controls duplicate `@font-face` removing; defaults to true
                    // removeDuplicateMediaBlocks: true, // controls duplicate `@media` removing; defaults to true
                    // removeDuplicateRules: true, // controls duplicate rules removing; defaults to true
                    // restructureRules: true // controls rule restructuring; defaults to false
                }

            }
        }
    ))
    .pipe(
        gulp.dest(
            pathToVendor + pathToCustomBundle + pathInsideCustomBundle + 'Resources/public/assets/edit/css/' +  pathToForeignBundle
        )
    )
});

var runGulp = true;

if (typeof arg.pathToVendor == 'undefined') {
    console.log("Please pass argument --pathToVendor to gulp");
    runGulp = false;
}

if (typeof arg.pathToForeignBundle == 'undefined') {
    console.log("Please pass argument --pathToForeignBundle to gulp");
    runGulp = false;
}

if (typeof arg.pathInsideForeignBundle == 'undefined') {
    console.log("Please pass argument --pathInsideForeignBundle to gulp");
    runGulp = false;
}

if (runGulp) {

    /**
     * Fly Willy fly ;)
     */
    gulp.task(
        'default',
        [
            'viewCss',
            'editCss'
        ],
        function () {
            gulp.watch(
                [
                    pathToVendor + pathToCustomBundle + pathInsideCustomBundle + 'Resources/private/assets/shared/scss/**/*.scss',
                    pathToVendor + pathToForeignBundle + pathInsideForeignBundle + '/Resources/private/assets/view/scss/*.scss',
                    pathToVendor + pathToForeignBundle + pathInsideForeignBundle + '/Resources/private/assets/view/import/**/*.scss',
                    pathToVendor + pathToForeignBundle + pathInsideForeignBundle + '/Resources/private/assets/shared/import/**/*.scss',
                ], ['viewCss'])
                .on('change', function (evt) {
                    console.log(
                        '[watcher] File ' + evt.path + ' was ' + evt.type + ', compiling...'
                    );
                });
            gulp.watch(
                [
                    pathToVendor + pathToCustomBundle + pathInsideCustomBundle + 'Resources/private/assets/shared/scss/**/*.scss',
                    pathToVendor + pathToForeignBundle + pathInsideForeignBundle + '/Resources/private/assets/edit/scss/*.scss',
                    pathToVendor + pathToForeignBundle + pathInsideForeignBundle + '/Resources/private/assets/edit/import/**/*.scss',
                    pathToVendor + pathToForeignBundle + pathInsideForeignBundle + '/Resources/private/assets/shared/import/**/*.scss',
                ], ['editCss'])
                .on('change', function (evt) {
                    console.log(
                        '[watcher] File ' + evt.path + ' was ' + evt.type + ', compiling...'
                    );
                });
        }
    );
}
