<?php

namespace KITT3N\Pimcore\CustomBundle;

use PackageVersions\Versions;
use Pimcore\Extension\Bundle\AbstractPimcoreBundle;

class Kitt3nPimcoreCustomBundle extends AbstractPimcoreBundle
{
    public function getJsPaths()
    {
        return [
            '/bundles/kitt3npimcorecustom/js/pimcore/startup.js'
        ];
    }

    /**
     * @inheritDoc
     */
    public function getVersion()
    {
        return Versions::getVersion('kitt3n/pimcore-custom');
    }
}
