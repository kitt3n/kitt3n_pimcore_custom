<?php

namespace KITT3N\Pimcore\CustomBundle\Controller;

use Pimcore\Config;
use Pimcore\Controller\FrontendController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class DefaultController extends \KITT3N\Pimcore\LayoutsBundle\Controller\DefaultController
{
    public function indexAction(Request $request)
    {
        return parent::indexAction($request);
    }

}
