pimcore.registerNS("pimcore.plugin.Kitt3nPimcoreCustomBundle");

pimcore.plugin.Kitt3nPimcoreCustomBundle = Class.create(pimcore.plugin.admin, {
    getClassName: function () {
        return "pimcore.plugin.Kitt3nPimcoreCustomBundle";
    },

    initialize: function () {
        pimcore.plugin.broker.registerPlugin(this);
    },

    pimcoreReady: function (params, broker) {
        // alert("Kitt3nPimcoreCustomBundle ready!");
    }
});

var Kitt3nPimcoreCustomBundlePlugin = new pimcore.plugin.Kitt3nPimcoreCustomBundle();
