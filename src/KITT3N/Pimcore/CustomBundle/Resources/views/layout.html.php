<!DOCTYPE html>

<?php

/**
 * @var array $aBamParams
 * @var \Pimcore\Model\DataObject\User $aBamParams ["oUser"]
 * @var \Pimcore\Model\Document\Page $aBamParams ["oDocument"]
 * @var \Pimcore\Config\Config         $aBamParams ["oWebsiteConfig"]
 * @var \Pimcore\Model\DataObject\User $aBamParams ["oUser"]
 * @var \Pimcore\Model\Document\Page $aBamParams ["oDocument"]
 * @var \Pimcore\Config\Config       $aBamParams ["oWebsiteConfig"]
 */

?>

<html lang="<?= array_key_exists('language', $aDocumentProperties) ? $aDocumentProperties["language"]->getData() : ''; ?>">
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta charset="UTF-8">
    <title><?= $oDocument->getTitle(); ?></title>
    <meta name="description" content="<?= $oDocument->getDescription(); ?>">

    <?= $this->template(
        "@Kitt3nPimcoreElementsBundle/Resources/views/Includes/Edit/Head/Script/vue.html.php"
    ); ?>

    <?= $this->template(
        "@Kitt3nPimcoreLayoutsBundle/Resources/views/Includes/Head/Script/functions.html.php"
    ); ?>

    <?= $this->template(
        "@Kitt3nPimcoreLayoutsBundle/Resources/views/Includes/Head/Script/lazyload.html.php"
    ); ?>

    <link rel="stylesheet" type="text/css" href="/bundles/kitt3npimcorecustom/assets/view/css/kitt3n/pimcore-layouts/view.min.css" />
    <link rel="stylesheet" type="text/css" href="/bundles/kitt3npimcorecustom/assets/view/css/kitt3n/pimcore-elements/view.min.css" />

    <?php if($this->editmode): ?>
        <link rel="stylesheet" type="text/css" href="/bundles/kitt3npimcorecustom/assets/edit/css/kitt3n/pimcore-layouts/edit.min.css" />
        <link rel="stylesheet" type="text/css" href="/bundles/kitt3npimcorecustom/assets/edit/css/kitt3n/pimcore-elements/edit.min.css" />
    <?php endif; ?>

</head>

    <?= $this->template(
        "@Kitt3nPimcoreLayoutsBundle/Resources/views/Includes/Body/index.html.php",
        [
            "oWebsiteConfig" => $oWebsiteConfig,
            "oDocument" => $oDocument,
            "oUser" => $oUser,
            "aRestrictions" => $aRestrictions,
            "aBamParams" => $aBamParams,
        ]
    ); ?>

</html>